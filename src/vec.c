/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/16 10:22:55 by bperreon          #+#    #+#             */
/*   Updated: 2015/10/16 12:13:18 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "matrix.h"

t_point3	point3(float x, float y, float z)
{
	t_point3	p;

	p.x = x;
	p.y = y;
	p.z = z;
	return (p);
}

float		vec_norm(t_point3 vec)
{
	return (sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z));
}

t_point3	vec_normalize(t_point3 vec)
{
	t_point3	n;
	float		norm;

	if (!(norm = vec_norm(vec)))
		return (point3(0, 0, 0));
	n.x = vec.x / norm;
	n.y = vec.y / norm;
	n.z = vec.z / norm;
	return (n);
}

t_point3    cross_product(t_point3 a, t_point3 b)
{
	t_point3   c;

	c.x = a.y*b.z - a.z*b.y;
	c.y = a.z*b.x - a.x*b.z;
	c.z = a.x*b.y - a.y*b.x;

	return c;
}

t_point3    vec_from(t_point3 a, t_point3 b)
{
	t_point3 c;
	
	c.x = b.x - a.x;
	c.y = b.y - a.y;
	c.z = b.z - a.z;

	return c;
}