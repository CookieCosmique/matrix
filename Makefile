# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bperreon <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/22 17:51:18 by bperreon          #+#    #+#              #
#    Updated: 2015/11/20 13:53:30 by bperreon         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = matrix.a
CC = gcc
CFLAGS = -Wall -Wextra -Werror
HEADERS = -I./include
SOURCE = ./src/matrix_transformation.c ./src/vec.c ./src/matrix_utils.c \
		 ./src/projection_matrix.c ./src/matrix_rotation.c
OBJ = $(SOURCE:.c=.o)

all: $(GLFW) $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

%.o: %.c include
	$(CC) $(CFLAGS) $(HEADERS) -c $< -o $@

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
